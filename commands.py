from database import clients_database, rooms_database
from dictionaries import (
    commands_dict,
    help_messages,
    notifications,
)


def register_command(*locations):
    def wrapper(func):
        for loc in locations:
            commands_dict[loc][func.__name__] = func
        return func

    return wrapper


@register_command("room")
def start_game(client_id: int, *args):
    if rooms_database.get_room_status_by_client(client_id) != "preparation":
        return "There are not enough players in the room.\n\n"
    if rooms_database.get_room_creator_by_player(client_id) != client_id:
        return "You do not have sufficient rights for this command.\n\n"
    rooms_database.start_game(client_id)
    return "Game started successfully.\n\n"


@register_command("general")
def helpme(client_addr: int, *args) -> str:
    return f"Available commands:\n{help_messages[clients_database.get_client_location_name(client_addr)]}\n"


@register_command("room")
def kick(client_addr: int, kicked_player, *args) -> str:
    if int(kicked_player) not in rooms_database.get_room_players_by_player_id(
        client_addr
    ):
        return f"User with ID {kicked_player} is not in this room\n\n"
    elif int(kicked_player) == rooms_database.get_room_creator_by_player(client_addr):
        return "You don't have enough rights\n\n"
    rooms_database.kick_player_from_his_room(int(kicked_player))
    return "Player kicks successfully\n\n"


@register_command("general")
def my_info(client_addr: int, *args) -> str:
    return clients_database.get_info_about_player(client_addr) + "\n\n"


@register_command("room")
def invite(client_addr: int, invited_player_id, *args) -> str:
    if len(rooms_database.get_room_players_by_player_id(client_addr)) >= 2:
        return "The room is already full, you can't invite a player\n\n"
    try:
        _ = clients_database.get_player_by_id(int(invited_player_id))
    except:
        return "User with this ID does not exist\n\n"
    notifications[int(invited_player_id)] = (
        f"User # {client_addr} invites you to the room to agree, enter:\n"
        f"- connect {clients_database.get_player_location_id(client_addr)}\n\n"
    )
    return "User invited successfully\n\n"


@register_command("server")
def connect(client_addr: int, room_id, password=None, *args) -> str:
    try:
        room = rooms_database.get_room_by_id(int(room_id))
    except:
        return "Such a room does not exist\n\n"
    if room.get_status() != "waiting":
        return "This room is not available\n\n"
    if room.get_password() is not None and room.get_password() != password:
        return "The password is incorrect\n\n"

    rooms_database.connect_player_to_room(client_addr, int(room_id))
    return f"You have successfully connected to the room\n\n"


@register_command("server")
def games_list(client_addr: int, *args) -> str:
    try:
        rooms = rooms_database.get_all_free_rooms()
    except Exception:
        return "No connection rooms available\n\n"
    return f"Rooms waiting for the player:\n{rooms}\n\n"


@register_command("room")
def leave_room(client_addr: int, *args) -> str:
    if rooms_database.get_room_creator_by_player(player=client_addr) == client_addr:
        rooms_database.remove_player_from_his_room_and_delete_this_room(
            player=client_addr
        )
    else:
        rooms_database.remove_player_from_his_room(player=client_addr)
    return f"You left the room.\n\n"


@register_command("server")
def create_room(client_addr: int, room_type="public", password=None) -> str:
    if room_type == "private":
        assert password is not None

    rooms_database.create_new_room(creator_id=client_addr, password=password)
    return f"Room # {clients_database.get_player_location_id(player=client_addr)} was created\n\n"


# class GetCommandData:
#     dict_key = String(minsize=3, maxsize=20)
#
#     def __init__(self, *args):
#         assert len(args) == 1, "Get command takes exactly 1 argument"
#         self.dict_key = args[0]
#

# @register_command(location="server")
# def get_value(*args):
#     get_data = GetCommandData(*args)
#     return shared_dict.get(get_data.dict_key, f"No value with key {get_data.dict_key}")
#
#
# class SetCommandData:
#     dict_key = String(minsize=3, maxsize=20)
#     dict_new_value = String(minsize=3, maxsize=100)
#
#     def __init__(self, *args):
#         assert len(args) == 2, "Get command takes exactly 2 arguments"
#         self.dict_key = args[0]
#         self.dict_new_value = args[1]
#
#
# @register_command(location="server")
# def set_value(*args):
#     set_data = SetCommandData(*args)
#     shared_dict[set_data.dict_key] = set_data.dict_new_value
#     return f"Successfully saved {set_data.dict_key}={set_data.dict_new_value}"
#
#
# @register_command(location="server")
# def bye(*args):
#     raise StopIteration("User entered bye command")
