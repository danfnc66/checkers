import abc
from typing import List

from classes import Location, Client, Room
from dictionaries import notifications


class DatabaseInterface(abc.ABC):
    pass


class DatabaseClientData(DatabaseInterface):
    def __init__(self):
        self._db = {}

    def get_client_location_name(self, client_id: int) -> str:
        return self._db[client_id].get_location().get_title()

    def get_player_location_id(self, player: int) -> int:
        return self._db[player].get_location().get_location_id()

    def set_player_location(self, player_id: int, new_location: tuple) -> None:
        self._db[player_id].set_location(
            Location(title=new_location[0], location_id=new_location[1])
        )

    def get_info_about_player(self, player: int) -> str:
        return f"ID - {player}\n" + "\n".join(
            [f"{name} - {value}" for name, value in self._db[player].get_all_info()]
        )

    def set_new_client(self, client_id):
        self._db[client_id] = Client()

    def get_player_by_id(self, player_id):
        return self._db[player_id]


class DatabaseRoom(DatabaseInterface):
    def __init__(self):
        self._db = {}

    def get_room_creator_by_player(self, player: int) -> int:
        return self._db[
            clients_database.get_player_by_id(player).get_location().get_location_id()
        ].get_creator_id()

    def get_room_players_by_player_id(self, client_addr: int) -> List[int]:
        return self._db[
            clients_database.get_player_by_id(client_addr)
            .get_location()
            .get_location_id()
        ].get_players()

    def get_room_by_id(self, room_id: int) -> dict:
        return self._db[room_id]

    def get_all_rooms(self) -> dict:
        return self._db

    def delete_player_from_room(self, client_id: int) -> None:
        self._db[
            clients_database.get_player_by_id(client_id)
            .get_location()
            .get_location_id()
        ].delete_player(client_id)

    def set_room_status_by_client(self, client_id: int, new_status: str) -> None:
        self._db[clients_database.get_player_location_id(client_id)].set_status(
            new_status
        )

    def get_room_status_by_client(self, client_id: int):
        return self._db[clients_database.get_player_location_id(client_id)].get_status()

    def delete_room_by_id(self, room_id: int) -> None:
        del self._db[room_id]

    def connect_player_to_room(self, player: int, room_id: int) -> None:
        self._db[int(room_id)].add_player(player)
        self._db[int(room_id)].set_status("preparation")
        clients_database.set_player_location(player, ("room", int(room_id)))
        notifications[
            self._db[int(room_id)].get_creator_id()
        ] = f"Player # {player} connected into your room\n\n"

    def get_all_free_rooms(self) -> str:
        return "\n".join(
            [
                f"Room # {key} - User # {value.get_creator_id()}\n"
                for key, value in self.get_all_rooms().items()
                if value.get_status() == "waiting"
            ]
        )

    def create_new_room(self, creator_id: int, password: str) -> None:
        rooms_count = len(self._db)
        self._db[rooms_count + 1] = Room(creator_id, password)
        clients_database.set_player_location(creator_id, ("room", rooms_count + 1))

    def kick_player_from_his_room(self, player: int) -> None:
        self.set_room_status_by_client(player, "preparation")
        self.delete_player_from_room(player)
        clients_database.set_player_location(player, ("server", None))
        notifications[player] = "You are kicked out of the room\n\n"

    def remove_player_from_his_room(self, player: int) -> None:
        self.set_room_status_by_client(player, "waiting")
        self.delete_player_from_room(player)
        clients_database.set_player_location(player, ("server", None))

    def remove_player_from_his_room_and_delete_this_room(self, player: int) -> None:
        loc_to_del = clients_database.get_player_location_id(player)
        for player in self.get_room_players_by_player_id(player):
            clients_database.set_player_location(player, ("server", None))
        self.delete_room_by_id(loc_to_del)

    def start_game(self, creator_id):
        self.set_room_status_by_client(creator_id, new_status="gaming")
        for player in self.get_room_players_by_player_id(creator_id):
            clients_database.set_player_location(
                player,
                new_location=("game", clients_database.get_player_location_id(player)),
            )
            if player != creator_id:
                notifications[player] = "Game has started.\n\n"


clients_database = DatabaseClientData()
rooms_database = DatabaseRoom()
