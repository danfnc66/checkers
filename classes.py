from typing import List, Union


class Room:
    def __init__(self, creator_id: int, password: Union[str, None]):
        self._creator_id = creator_id
        self._password = password
        self._status = "waiting"
        self._players = [
            creator_id,
        ]

    def get_password(self) -> Union[str, None]:
        return self._password

    def get_status(self) -> str:
        return self._status

    def set_status(self, new_status: str) -> None:
        self._status = new_status

    def add_player(self, new_player_id: int) -> None:
        self._players.append(new_player_id)

    def delete_player(self, player_to_del: int) -> None:
        self._players.remove(player_to_del)

    def get_creator_id(self) -> int:
        return self._creator_id

    def get_players(self) -> List[int]:
        return self._players


class Location:
    def __init__(self, title: str, location_id: int = None):
        self._title = title
        self._location_id = location_id

    def get_title(self) -> str:
        return self._title

    def get_location_id(self) -> int:
        return self._location_id

    def get_title_and_location_id(self) -> tuple:
        return self._title, self._location_id

    def set_title_and_location_id(self, new_title: str, new_loc_id: int = None) -> None:
        self._title, self._location_id = new_title, new_loc_id


class Client:
    def __init__(
        self,
        location: Location = Location(title="server"),
        victories_count: int = 0,
        losses_count: int = 0,
    ):
        self._location = location
        self._victories_count = victories_count
        self._losses_count = losses_count
        self._friends = []

    def add_friend(self, new_friend_id):
        self._friends.append(new_friend_id)

    def get_all_info(self) -> tuple:
        return (
            ("Location", self._location),
            ("Victories count", self._victories_count),
            ("Losses count", self._losses_count),
            ("Friends", self._friends),
        )

    def set_location(self, new_location: Location) -> None:
        self._location = new_location

    def get_location(self):
        return self._location
