commands_dict = {"server": {}, "room": {}, "general": {}}
notifications = {}
help_messages = {
    "server": "- create_room <private or public> <password if private> - create new room\n"
    "- games_list - get all available rooms\n"
    "- connect <id> <password if room is private>\n",
    "room": "- leave_room - leave the room\n" "- invite <player_id>\n",
}
