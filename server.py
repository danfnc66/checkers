import os
import socket
import threading
from concurrent.futures import ThreadPoolExecutor

from database import clients_database
from dictionaries import commands_dict, help_messages, notifications
from settings import HOST, PORT, CONNECTIONS_NUMBER

# No inspection is added because these imports register command functions
# noinspection PyUnresolvedReferences
from commands import create_room, leave_room, games_list, connect, invite


def handle_command(client_id, cmd, *args):
    try:
        command_func = commands_dict[
            clients_database.get_client_location_name(client_id)
        ].get(cmd)
        if command_func is None:
            command_func = commands_dict["general"].get(cmd)
        if command_func is None:
            raise ValueError("Unknown command\n\n")
        result = command_func(client_id, *args)
    except (TypeError, ValueError, AssertionError) as e:
        result = str(e)
    except StopIteration:
        result = ""
    return result


def handle_client(conn, client_id):
    print(f"Using thread {threading.get_ident()} for client: {client_id}")
    # clients_data[client_id] = Client()
    clients_database.set_new_client(client_id)
    notifications[client_id] = "\n"
    read_file = conn.makefile(mode="r", encoding="utf-8")
    write_file = conn.makefile(mode="w", encoding="utf-8")
    write_file.write(
        f"Available commands:\n{help_messages[clients_database.get_client_location_name(client_id)]}\n"
    )
    write_file.flush()
    cmd = read_file.readline().strip()
    while cmd != "exit":
        if cmd != "":
            result = handle_command(client_id, *cmd.split())
            write_file.write(result)
            write_file.flush()
        else:
            write_file.write(notifications[client_id])
            write_file.flush()
            notifications[client_id] = "\n"
        cmd = read_file.readline().strip()
    conn.close()


def main():
    host = HOST
    port = PORT
    print(f"Started process with PID={os.getpid()}")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # solution for "[Error 89] Address already in use". Use before bind()
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(CONNECTIONS_NUMBER)
        #  Proper threads termination doesn't work here, use SIGKILL
        with ThreadPoolExecutor(max_workers=2) as executor:
            while True:
                conn, addr = s.accept()
                executor.submit(handle_client, conn, addr[1])


if __name__ == "__main__":
    main()
